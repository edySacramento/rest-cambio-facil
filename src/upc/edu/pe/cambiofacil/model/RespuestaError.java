package upc.edu.pe.cambiofacil.model;

import upc.edu.pe.cambiofacil.enumeration.MensajeErrorEnum;

public class RespuestaError {

	String codigo;
	String mensaje;
	
	public RespuestaError(MensajeErrorEnum error) {
		super();
		this.codigo = error.getCodigo();
		this.mensaje = error.getDescripcion();
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
