package upc.edu.pe.cambiofacil.model;

import java.util.List;

public class Sesion {

	private Usuario usuario;
	private List<CasaCambio> casasCambio;
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public List<CasaCambio> getCasasCambio() {
		return casasCambio;
	}
	public void setCasasCambio(List<CasaCambio> casasCambio) {
		this.casasCambio = casasCambio;
	}
}
