package upc.edu.pe.cambiofacil.model;

public class CasaCambio {

	private String nroOficina;
	private String direccion;
	private TipoCambio tipoCambio;
	
	public String getNroOficina() {
		return nroOficina;
	}
	public void setNroOficina(String nroOficina) {
		this.nroOficina = nroOficina;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public TipoCambio getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(TipoCambio tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
}
