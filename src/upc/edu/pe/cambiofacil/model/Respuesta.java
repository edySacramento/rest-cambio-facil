package upc.edu.pe.cambiofacil.model;

import upc.edu.pe.cambiofacil.enumeration.MensajeEnum;

public class Respuesta {

	private String codigo;
	private String mensaje;
	
	public Respuesta(MensajeEnum obj) {
		super();
		this.codigo = obj.getCodigo();
		this.mensaje = obj.getDescripcion();
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
