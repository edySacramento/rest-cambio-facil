package upc.edu.pe.cambiofacil.model;

public class TipoCambio {

	private String valorCompra;
	private String valorVenta;
	
	public String getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(String valorCompra) {
		this.valorCompra = valorCompra;
	}
	public String getValorVenta() {
		return valorVenta;
	}
	public void setValorVenta(String valorVenta) {
		this.valorVenta = valorVenta;
	}
}
