package upc.edu.pe.cambiofacil.enumeration;

public enum MensajeErrorEnum {

	USER_NOT_FOUND("E001","Usuario incorrecto"),
	PASSWORD_INCORRECT("E002","Constraseña incorrecta"),
	USER_EXIST("E003","Usuario ya se encuentra registrado");
	
	
	private String codigo;
	private String descripcion;

	private MensajeErrorEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
}
