package upc.edu.pe.cambiofacil.enumeration;

public enum MensajeEnum {

	SAVE_USER_OK("L001","Usuario registrado exitosamente"),
	LOGIN_USER_OK("L002","Usuario inicio sesion exitosamente"),
	SAVE_OK("L003","Registro Exitoso");

	private String codigo;
	private String descripcion;

	private MensajeEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
}
