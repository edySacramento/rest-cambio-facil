package upc.edu.pe.cambiofacil.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import upc.edu.pe.cambiofacil.enumeration.MensajeEnum;
import upc.edu.pe.cambiofacil.enumeration.MensajeErrorEnum;
import upc.edu.pe.cambiofacil.model.CasaCambio;
import upc.edu.pe.cambiofacil.model.Respuesta;
import upc.edu.pe.cambiofacil.model.Usuario;
import upc.edu.pe.cambiofacil.util.Utilitario;
import upc.edu.pe.cambiofacil.model.RespuestaError;
import upc.edu.pe.cambiofacil.model.Sesion;

@Path("/service")
public class ServiceRest {
	
	static Usuario userLogin = new Usuario();
	static List<Usuario> listUser = new ArrayList<Usuario>();
	static List<CasaCambio> listCasaCambio = new ArrayList<CasaCambio>();
	static List<Sesion> sesiones = new ArrayList<Sesion>();
	
	@POST
	@Path("/save-user")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveUser(Usuario obj) {
		Usuario user = Utilitario.buscarUsuario(listUser, obj.getMail());
		
		if (user != null) {
			return Response.status(400).entity(new RespuestaError(MensajeErrorEnum.USER_EXIST)).build();
		}
		
		listUser.add(obj);
		return Response.status(200).entity(new Respuesta(MensajeEnum.SAVE_USER_OK)).build();
	}
	
	@GET
	@Path("/list-users")
	@Produces({MediaType.APPLICATION_JSON})
	public List<Usuario> listUser() {
		return listUser;
	}
	
	@POST
	@Path("/login")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response login(Usuario obj) {
		Usuario user = Utilitario.buscarUsuario(listUser, obj.getMail());
		
		if (user == null) {
			return Response.status(400).entity(new RespuestaError(MensajeErrorEnum.USER_NOT_FOUND)).build();
		}
		
		if (!user.getPassword().equals(obj.getPassword())) {
			return Response.status(400).entity(new RespuestaError(MensajeErrorEnum.PASSWORD_INCORRECT)).build();
		}
		
		userLogin = user;
	
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objInfo = mapper.createObjectNode();
		objInfo.put("mensaje", MensajeEnum.LOGIN_USER_OK.getDescripcion());

		ObjectNode objName = mapper.createObjectNode();
		objName.put("mail", user.getMail());
		objName.put("nombres", user.getNombres());
		objInfo.put("usuario", objName);
		
		return Response.status(200).entity(objInfo.toString()).build();
	}
	
	/*********************************************************/
	
	@POST
	@Path("/save-casa-cambio")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveCasaCambio(CasaCambio obj) {
		listCasaCambio.add(obj);
		return Response.status(200).entity(new Respuesta(MensajeEnum.SAVE_OK)).build();
	}
	
	@GET
	@Path("/list-casa-cambio")
	@Produces({MediaType.APPLICATION_JSON})
	public List<CasaCambio> listCasaCambio() {
		return listCasaCambio;
	}
}
