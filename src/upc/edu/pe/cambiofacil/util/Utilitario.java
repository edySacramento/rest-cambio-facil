package upc.edu.pe.cambiofacil.util;

import java.util.List;

import upc.edu.pe.cambiofacil.model.Usuario;

public class Utilitario {

	public static Usuario buscarUsuario(List<Usuario> list, String mail){
		for (Usuario usuario : list) {
			if (usuario.getMail().equals(mail)) {
				return usuario;
			}
		}
		return null;
	}
}
